﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using Exception = System.Exception;

namespace ApiTestTd
{
    public class ValuesController : ApiController
    {
        // GET api/values  
        public IHttpActionResult Get()
        {
            return Ok(Singleton.Instance.Dao.FindAll()); //Return ok(...) because there is a return in this method
            
        }

        // GET api/values/5 
        public IHttpActionResult Get(int id)
        {
            return Ok(Singleton.Instance.Dao.FindOne(id));
        }

        // POST api/values 
        public IHttpActionResult Post(BooksModel books)
        {
            Singleton.Instance.Dao.InsertOne(books); //Return Ok() is after Because no return
            return Ok();
        }

        // PUT api/values/5 
        public IHttpActionResult Put(BooksModel books)
        {   
            Singleton.Instance.Dao.UpdateOne(books);
            return Ok();
        }

        // DELETE api/values/5 
        public IHttpActionResult Delete(int id)
        {
            Singleton.Instance.Dao.DeleteOne(id);
            return Ok();
        }
    }
}