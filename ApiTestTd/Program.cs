﻿using Microsoft.Owin.Hosting;
using System;
using System.Diagnostics;
using System.Net.Http;
using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using MongoDB.Driver;

namespace ApiTestTd
{
    public class Program
    {
        static void Main()
        {
            string baseAddress = "http://localhost:9000/";

            //MongoConnect MongoConnect = new MongoConnect();

            var mongoClient = new MongoClient("mongodb://localhost:27017");
            var database = mongoClient.GetDatabase("local");
            var mongoCollection = database.GetCollection<BooksModel>("bibliotheque");

            /*
             * Singleton for difference between test and prod
             */
            Singleton.Instance.SetCollection(mongoCollection);
            Singleton.Instance.SetDao(new BooksDAO());

            // Start my webApp called Startup
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                /*
                 * Create HttpCient and make a request to api/values
                */
                HttpClient client = new HttpClient();

                var response = client.GetAsync(baseAddress + "api/values").Result;



                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                var version = fvi.FileVersion;



                /*
                 * Return in console the response from the database and add Console.Readline to wait an action from user 
                 */
                Console.WriteLine(version);
                Console.ReadLine();
            }
        }
    }
}