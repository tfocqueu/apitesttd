﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ApiTestTd.Model;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ApiTestTd.MongoDb
{
    public class BooksDAO : Idao
    {
        public async Task<BooksModel> FindOne(int number)
        {
            var query = await Singleton.Instance.Collection.Find(book => book.Number == number).ToListAsync();

            return query.FirstOrDefault();

        }

        public async void InsertOne(BooksModel book)
        {
            await Singleton.Instance.Collection.InsertOneAsync(book);
        }

        public async void DeleteOne(int number)
        {
            await Singleton.Instance.Collection.DeleteOneAsync(book => book.Number == number);
        }

        public async void UpdateOne(BooksModel book)
        {
            await Singleton.Instance.Collection.FindOneAndUpdateAsync(
                Builders<BooksModel>.Filter.Eq(b => b.Number, book.Number),
                Builders<BooksModel>.Update.Set(b => b.Number, book.Number)
                .Set(b =>b.Title, book.Title)
                .Set(b => b.Author , book.Author)
                );
        }

        public async Task<List<BooksModel>>FindAll()
        {
            return await Singleton.Instance.Collection.Find(new BsonDocument()).ToListAsync();
            
        }

    }
}
