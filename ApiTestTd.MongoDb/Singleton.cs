﻿using ApiTestTd.Model;
using MongoDB.Driver;

namespace ApiTestTd.MongoDb
{
    public class Singleton
    {
        private static Singleton _instance;
        public Idao Dao { get; private set; }
        public IMongoCollection<BooksModel> Collection { get; private set; }

        /*
         * This method is for TestController
         */
        public void SetDao(Idao dao)
        {
            Dao = dao;
        }

        /*
         * This method is for the TestMongo
         */
        public void SetCollection( IMongoCollection<BooksModel> collection)
        {
            Collection = collection;
        }

        /* If _instance is null then we add an instance and if is not null we return _instance */
        public static Singleton Instance
        {
            get { return _instance ?? (_instance = new Singleton()); }
        }
    }
}
