﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiTestTd.Model;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ApiTestTd.MongoDb
{
    public class MongoConnect
    {

        /*
         * I don't know how to call this _construct in my programs.cs
         */

        public MongoConnect()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            var database = mongoClient.GetDatabase("local");
            var mongoCollection = database.GetCollection<BooksModel>("bibliotheque");
        }
    }
}
