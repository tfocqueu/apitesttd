﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiTestTd;
using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using MongoDB.Bson;
using MongoDB.Driver;
using NSubstitute;
using NUnit.Framework;

namespace ApiTestTdTest
{
    [TestFixture]
    public class TestMongo
    {
        private IMongoCollection<BooksModel> _collection;
        private BooksDAO _bookDao;

        /*
         * SetUp is call at the beginning of all tests
         */
        [SetUp]
        public void SetUp()
        {
            _collection = Substitute.For<IMongoCollection<BooksModel>>();
            Singleton.Instance.SetCollection(_collection);
            _bookDao = new BooksDAO();
        }

        /*
         * TearDown is call at the end of all test
         */
        [TearDown]
        public void TearDown()
        {
            _collection.ClearReceivedCalls();
        }

        /*
         * Test if The function InsertOneAsync is called.
         */
        [Test]
        public void TestInsertOne()
        {

            _bookDao.InsertOne(new BooksModel());
            _collection.Received(1).InsertOneAsync(Arg.Any<BooksModel>());
        }

        /*
         * Test if The function Find is called.
         */
        [Test]
        public async Task TestFindOne()
        {
            await _bookDao.FindOne(2);
            _collection.Received(1).Find(book => book.Number == Arg.Any<int>());
        }

        [Test]
        public async Task TestFindAll()
        {
            await _bookDao.FindAll();
            _collection.Received(1).Find(new BsonDocument());
        }

        /*
         * Test if The function DeleteOneAsync is called. 
         */
        [Test]
        public void TestDeleteOne()
        {
            _bookDao.DeleteOne(2);
            var t = _collection.ReceivedCalls();

            Assert.AreEqual(1, t.Count(call => call.GetMethodInfo().Name == "DeleteOneAsync"));
        }

        /*
         * Test if The function FindOneAndUpdateAsync is called.
         */
        [Test]
        public void TestUpdate()
        {
            _bookDao.UpdateOne(new BooksModel());

            var t = _collection.ReceivedCalls();
            Assert.AreEqual(1, t.Count(call => call.GetMethodInfo().Name == "FindOneAndUpdateAsync"));
        }
    }
}