﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ApiTestTd;
using ApiTestTd.Model;
using ApiTestTd.MongoDb;
using NSubstitute;
using NUnit.Framework;

namespace ApiTestTdTest
{
    [TestFixture]
    public class TestController
    {
        private Idao _dao;
        private ValuesController _controller;

        /*
         * SetUp is call at the beginning of all tests
         */
        [SetUp]
        public void SetUp()
        {
            _dao = Substitute.For<Idao>(); //Create a substitute to add a fake value
            Singleton.Instance.SetDao(_dao); //Call the singleton to set Instance
            _controller = new ValuesController(); //Instance the controller to call method
        }

        /*
         * TearDown is call at the end of all test
         */
        [TearDown]
        public void TearDown()
        {
            _dao.ClearReceivedCalls(); //Clear function Received after each test to start new test clean
        }

        /*
         * Test if The function FindOne is called in the controller.
         */
        [Test]
        public void TestFindOne()
        {
            _controller.Get(2); //Call function Get in controller (Int parameter)

            _dao.Received(1).FindOne(Arg.Any<int>()); //1 is used to count the number of the call to the function get
        }

        /*
         * Test if The function UpdateOne is called in the controller.
         */
        [Test]
        public void TestUpdateOne()
        {
            _controller.Put(new BooksModel()); //Call function Put in controller (new Book parameter)
            _dao.Received(1).UpdateOne(Arg.Any<BooksModel>()); //Arg.Any is used to specify any Bookmodel
            
        }

        /*
         * Test if The function DeleteOne is called in the controller.
         */
        [Test]
        public void TestDeleteOne()
        {
            _controller.Delete(1); //Call function Deletee in controller (Int parameter)
            _dao.Received(1).DeleteOne(Arg.Any<int>());
        }

        /*
         * Test if The function InsertOne is called in the controller.
         */
        [Test]
        public void TestInsertOne()
        {
            _controller.Post(new BooksModel()); //Call function Post in controller (new Book parameter)

            _dao.Received(1).InsertOne(Arg.Any<BooksModel>());
        }

        /*
         * Test if The function FindAll is called in the controller.
         */
        [Test]
        public void TestFindAll()
        {
            _controller.Get(); //Call function Get in controller

            _dao.Received(1).FindAll();
        }
    }
}
